﻿using System;

namespace DigitsSum
{
    class Program
    {
        // Намерете сумата на цифрите
        // на дадено трицифрено число.
        static void Main(string[] args)
        {
            int number = 649;
            int digit1 = number % 10;
            int digit2 = number / 10 % 10;
            int digit3 = number / 100;

            int sum = digit1 + digit2 + digit3;

            Console.WriteLine(sum);
        }
    }
}
