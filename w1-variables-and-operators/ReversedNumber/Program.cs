﻿using System;

namespace ReversedNumber
{
    class Program
    {
        //Да се състави програма, която по дадено цяло трицифрено число
        //извежда числото записано със същите цифри в обратен ред.
        static void Main(string[] args)
        {
            Console.Write("Въведи трицифрено число: ");
            int number = int.Parse(Console.ReadLine());
            int firstDigit = number % 10;
            int secondDigit = number / 10 % 10;
            int thirdDigit = number / 100;
            int reversedNumber = firstDigit * 100
                + secondDigit * 10
                + thirdDigit;
            Console.WriteLine("Обърнатото: " + reversedNumber);
        }
    }
}
