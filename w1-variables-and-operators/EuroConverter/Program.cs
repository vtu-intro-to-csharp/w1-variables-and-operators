﻿using System;

namespace EuroConverter
{
    class Program
    {
        // По въведено число на сума в лева
        // изведете сумата в евро.
        // За 1 евро = 1.95583 лв.
        static void Main(string[] args)
        {
            const decimal euroRate = 1.95583m;

            Console.WriteLine("Въведете сума в лева: ");
            decimal bgn = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine(bgn + " лв. = ");
            Console.WriteLine(bgn / euroRate + "EUR");
        }
    }
}
